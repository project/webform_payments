<?php

/*************************************************************
 * DRUPAL HOOKS
 */

/**
 * Implementation of hook_menu().
 */
function webform_payments_menu($may_cache) {
  if ($may_cache) {
    $items[] = array(
      'type' => MENU_CALLBACK,
      'path' => 'webform_payments/gateway',
      'title' => t('Connecting to PayPal'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('webform_payments_do'),
      'access' => TRUE,
    );
    $items[] = array(
      'path' => 'admin/settings/webform_payments',
      'title' => t('Webform PayPal'),
      'description' => t('These settings control the configurable global options for Webform PayPal.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('webform_payments_admin_settings'),
      'access' => user_access('administer site configuration'),
      'type' => MENU_NORMAL_ITEM, // optional
    );

  }
  
  return $items;  
}

/**
 * Implementation of hook_form_alter().
 */
function webform_payments_form_alter($form_id, &$form) {
  $nid = $form['details']['nid']['#value'];
  switch ($form_id) {
    case 'webform_components_form':
      $form['#theme'] = 'webform_payments_webform_components_form';
      $node = $form['#node'];
      $components = $node->webform['components'];

      $qs = db_query("SELECT wp.webform_use, wp.webform_component FROM {webform_payments} wp WHERE wp.nid = %d", $node->nid);
      while ($obj = db_fetch_object($qs)) {
        $paypal[$obj->webform_component] = $obj->webform_use;
      }

      foreach($form['components'] as $key => $component) {
        $form['components'][$key]['paypal'] = array(
          '#type' => 'checkbox',
          '#default_value' => $paypal[$key],
        );
      }

      $form['add']['paypal'] = array(
        '#type' => 'checkbox',
        '#attributes' => array('class' => 'webform-paypal'),
      ); 

      $form['#submit']['webform_payments_webform_components_form_submit'] = array();

      break;
    case 'webform_component_edit_form':
      // Figure out if the PayPal checkbox should be set by default.
      $paypal = FALSE;
      if ($_REQUEST['webform_payments_add_component_paypal']) {
        unset($_REQUEST['webform_payments_add_component_paypal']);
        $paypal = TRUE;
      } else {
        $cid = $form['cid']['#value'];
        $nid = $form['nid']['#value'];
        $paypal = db_result(db_query("SELECT wp.webform_use FROM {webform_payments} wp WHERE wp.webform_component = %d AND wp.nid = %d", $cid, $nid));
//        rdpw($form);
      }
      
      $form['webform_payments'] = array(
        '#type' => 'fieldset',
        '#title' => t('PayPal settings'),
        '#collapsed' => !$paypal,
        '#collapsible' => TRUE,
      );
      
      $form['webform_payments']['webform_payments_use'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add the value of this element to the total sent to PayPal for checkout.'),
        '#description' => t('If the value of this form is not numeric, it will be excluded automatically.'),
        '#default_value' => $paypal,
      );
      
      $form['#submit']['webform_payments_webform_component_edit_form_submit'] = array();
      
    case 'webform_client_form_' . $nid:
      if (!$nid) {
        $nid = $form['nid']['#value'];
      }
      $node = node_load($nid);
      if ($node->webform_payments_use) {
        $form['webform_payments_title'] = array(
          '#type' => 'hidden',
          '#value' => $node->title,
        );
        
      }
      
        //Rather than just throwing our function on the end of #submit,
        //we have to put it in first because of the way the original
        //submit function works. If we don't, our new function will not
        //be called.
        if ($node->webform_payments_use) {
          $func['webform_payments_webform_client_form_submit'] = array();
          $form['#submit'] = array_merge($func, $form['#submit']);
        
          $func['webform_payments_webform_client_form_validate'] = array();
          $form['#validate'] = array_merge($func, $form['#validate']);
          $form['help'] = array(
            '#value' => t('<div style="color: red">To pay with your credit card, <b>be sure to click on the  "Continue" link</b> in the left column when you get to the PayPal site.</div>'),
            '#weight' => 1000,
          );
        }
      break;
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function webform_payments_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) { 
    case 'insert':
    case 'update':
      db_query("DELETE FROM {webform_payments} WHERE nid = %d", $node->nid);
      db_query("INSERT INTO {webform_payments} (nid, webform_use, webform_component) VALUES (%d, %d, %d)", $node->nid, $node->webform_payments_use, $node->webform_payments_component);
      break;
    case 'delete':
      db_query("DELETE FROM {webform_payments} WHERE nid = %d", $node->nid);
      break;
    case 'prepare':
    case 'load':
      $node->webform_payments_use = 0;
      $qs = db_query("SELECT * FROM {webform_payments} wp WHERE wp.nid = %d", $node->nid);
      while ($obj = db_fetch_object($qs)) {
        if ($node->webform_payments_use == 0) {
          $node->webform_payments_use = $obj->webform_use;
        }
        $node->webform_payments_component = $obj->webform_component; 
      }
      break;
  }
}

/*************************************************************
 * THEME FUNCTIONS
 */

function theme_webform_payments_webform_components_form($form) {
  // Add CSS to display submission info. Don't preprocess because this CSS file is used rarely.
  drupal_add_css(drupal_get_path('module', 'webform') .'/webform.css', 'module', 'all', FALSE);
  drupal_add_js(drupal_get_path('module', 'webform') .'/webform.js', 'module', 'header', FALSE, TRUE, FALSE);

  $node = $form['#node'];

  $headers = array(t('Name'), t('Type'), t('Value'), t('Mandatory'), t('E-mail'), t('PayPal'), t('Weight'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();

  // Add a row containing form elements for a new item.
  unset($form['add']['name']['#title'], $form['add_type']['#description']);
  $form['add']['name']['#value'] = t('New component name');
  $form['add']['name']['#attributes']['class'] = 'webform-default-value';
  $form['add']['cid']['#attributes']['class'] = 'webform-cid';
  $form['add']['pid']['#attributes']['class'] = 'webform-pid';
  $form['add']['weight']['#attributes']['class'] = 'webform-weight';

  $row_data = array(
    drupal_render($form['add']['name']),
    drupal_render($form['add']['type']),
    '',
    drupal_render($form['add']['mandatory']),
    drupal_render($form['add']['email']),
    drupal_render($form['add']['paypal']),
    drupal_render($form['add']['cid']) . drupal_render($form['add']['pid']) . drupal_render($form['add']['weight']),
    array('colspan' => 3, 'data' => drupal_render($form['add']['add'])),
  );
  $add_form = array('data' => $row_data, 'class' => 'draggable webform-add-form');
  $form_rendered = FALSE;

  if (!empty($node->webform['components'])) {
    $component_tree = array();
    $page_count = 1;
    _webform_components_tree_build($node->webform['components'], $component_tree, 0, $page_count);
    $component_tree = _webform_components_tree_sort($component_tree);
    // Build the table rows.
    function _webform_components_form_rows($node, $cid, $component, $level, &$form, &$rows, &$add_form) {
      // Create presentable values.
      if (strlen($component['value']) > 30) {
        $component['value'] = substr($component['value'], 0, 30);
        $component['value'] .= "...";
      }
      $component['value'] = check_plain($component['value']);

      // Remove individual titles from the mandatory and weight fields.
      unset($form['components'][$cid]['mandatory']['#title']);
      unset($form['components'][$cid]['pid']['#title']);
      unset($form['components'][$cid]['paypal']['#title']);
      unset($form['components'][$cid]['weight']['#title']);
      unset($form['components'][$cid]['email']['#title']);

      // Add special classes for weight and parent fields.
      $form['components'][$cid]['cid']['#attributes']['class'] = 'webform-cid';
      $form['components'][$cid]['pid']['#attributes']['class'] = 'webform-pid';
      $form['components'][$cid]['weight']['#attributes']['class'] = 'webform-weight';

      // Build indentation for this row.
      $indents = '';
      for ($n = 1; $n <= $level; $n++) {
        $indents .= '<div class="webform-indentation">&nbsp;</div>';
      }

      // Add each component to a table row.
      $row_data = array(
        $indents . $component['name'],
        $component['type'],
        ($component['value'] == "") ? "-" : $component['value'],
        drupal_render($form['components'][$cid]['mandatory']),
        drupal_render($form['components'][$cid]['email']),
        drupal_render($form['components'][$cid]['paypal']),
        drupal_render($form['components'][$cid]['cid']) . drupal_render($form['components'][$cid]['pid']) . drupal_render($form['components'][$cid]['weight']),
        l(t('Edit'), 'node/'. $node->nid .'/edit/components/'. $cid),
        l(t('Clone'), 'node/'. $node->nid .'/edit/components/'. $cid .'/clone'),
        l(t('Delete'), 'node/'. $node->nid .'/edit/components/'. $cid .'/delete'),
      );
      $row_class = 'draggable' . ($component['type'] != 'fieldset' ? ' tabledrag-leaf' : '');
      $rows[] = array('data' => $row_data, 'class' => $row_class);
      if (isset($component['children']) && is_array($component['children'])) {
        foreach ($component['children'] as $cid => $component) {
          _webform_components_form_rows($node, $cid, $component, $level + 1, $form, $rows, $add_form);;
        }
      }

      // Add the add form if this was the last edited component.
      if (isset($_GET['cid']) && $component['cid'] == $_GET['cid'] && $add_form) { 
        $add_form['data'][0] = $indents . $add_form['data'][0];
        $rows[] = $add_form;
        $add_form = FALSE;
      }
    }
    foreach ($component_tree['children'] as $cid => $component) {
      _webform_components_form_rows($node, $cid, $component, 0, $form, $rows, $add_form);
    }
  }
  else {
    $rows[] = array(array('data' => t("No Components, add a component below."), 'colspan' => 9));
  }

  // Append the add form if not already printed.
  if ($add_form) {
    $rows[] = $add_form;
  }

  $output = '';
  $output .= theme('table', $headers, $rows, array('id' => 'webform-components'));
  $output .= drupal_render($form);
  return $output;
}

/*************************************************************
 * webform_payments_ADMIN_SETTINGS
 */
function webform_payments_admin_settings() {
  $form['webform_payments_account'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('webform_payments_account', NULL),
    '#title' => t('PayPal account'),
    '#description' => t('Enter the e-mail address associated with the PayPal account you wish to use with this site.'),
  );
  
  $form['trademark'] = array(
    '#type' => 'item',
    '#value' => t(' "PayPal.com", "PayPal", and all related logos, products and services described in its website are either trademarks or registered trademarks of PayPal or its licensors.'),
  );
  
  return system_settings_form($form);
}

/*************************************************************
 * webform_payments_DO
 */

function webform_payments_do() {
  $form_values = $_REQUEST['webform_payments_construct'];
  unset($_REQUEST['wg_paypal_construct']);
  
  $account_email = variable_get('webform_payments_account', NULL);
  $amount = (int) urldecode($_REQUEST['totals']);
  $item_name = urlencode(check_plain(urldecode($_REQUEST['paypal_title'])));
  
$host = "www.paypal.com";
$path = "/cgi-bin/webscr";
$data  = "cmd=_xclick&business=$account_email&item_name=$item_name&item_number=1&amount=$amount&";
$data .= "no_shipping=0&no_note=1&currency_code=USD&lc=US&bn=PP-BuyNowBF";
//$data = urlencode($data);

//header("POST $host$path HTTP/1.1\r\n" );
//header("Host: $host\r\n" );
//header("Content-type: application/x-www-form-urlencoded\r\n" );
//header("Content-length: " . strlen($data) . "\r\n" );
//header("Connection: close\r\n\r\n" );
//header($data);
header("Location:https://{$host}{$path}?$data");
/*  $form = array(
    '#action' => 'https://www.paypal.com/cgi-bin/webscr',
    'cmd' => array(
      '#type' => 'hidden',
      '#value' => '_xclick',
    ),
    'business' => array(
     '#type' => 'hidden',
     '#value' => $account_email,
    ),
    'item_name' => array(
      '#type' => 'hidden',
      '#value' => $form_values['webform_payments_title'],
    ),
    'item_number' => array(
      '#type' => 'hidden',
      '#value' => 1,
    ),
    'amount' => array(
      '#type' => 'hidden',
      '#value' => $amount,
    ),
    'no_shipping' => array(
      '#type' => 'hidden',
      '#value' => 0,
    ),
    'no_note' => array(
      '#type' => 'hidden',
      '#value' => 1,
    ),
    'currency_code' => array(
      '#type' => 'hidden',
      '#value' => 'USD',
    ),
    'lc' => array(
      '#type' => 'hidden',
      '#value' => 'US',
    ),
    'bn' => array(
      '#type' => 'hidden',
      '#value' => 'PP-BuyNowBF',
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Continue to PayPal'),
    ),
    'help' => array(
      '#value' => t('<div style="color: red">To pay with your credit card, <b>be sure to click on the "Continue" link</b> in the left column when you get to the PayPal site.</div>'),
    ),
  );
*/
//  drupal_add_js(drupal_get_path('module', 'webform_payments') .'/webform_payments.js');

  return $form;
 
}

/*************************************************************
 * WEBFORM_COMPONENTS_FORM
 */

/**
 * webform_payments submit function for webform_components_form.
 */
function webform_payments_webform_components_form_submit($form_id, $form_values) {
  switch($form_values['op']) {
    case t('Submit'):
    case t('Publish'):
      db_query("SELECT * FROM {webform_payments} WHERE nid = %d", $form_values['nid']);
      foreach ($form_values['components'] as $component) {
        if ($component['paypal']) {
          db_query("INSERT INTO {webform_payments} (nid, webform_use, webform_component) VALUES (%d, %d, %d)", $form_values['nid'], $component['paypal'], $component['cid']); 
        }
      }
      break;
    case t('Add'):
      // If we are adding a new component, pass the setting for the PayPal column to the component configuration form.
      if ($form_values['add']['paypal']) {
        $_SESSION['webform_payments_add_component_paypal'] = TRUE;
      }
      break;
  }
}

/*************************************************************
 * WEBFORM_COMPONENT_EDIT_FORM
 */

/**
 * webform_payments submit function for webform_component_edit_form.
 */
function webform_payments_webform_component_edit_form_submit($form_id, $form_values) {
  $cid = NULL;
  $cid = db_result(db_query("SELECT wc.cid FROM {webform_component} wc WHERE wc.nid = %d AND wc.form_key = '%s'", $form_values['nid'], $form_values['form_key']));
  if (!empty($cid)) {
    db_lock_table('webform_payments');
    db_query("DELETE FROM {webform_payments} WHERE nid = %d AND webform_component = %d", $form_values['nid'], $cid);
    db_query("INSERT INTO {webform_payments} (nid, webform_use, webform_component) VALUES (%d, %d, %d)", $form_values['nid'], $form_values['webform_payments']['webform_payments_use'], $cid);
    db_unlock_tables('webform_payments');
  }  
}

/*************************************************************
 * WEBFORM_CLIENT_FORM
 */

/**
 * webform_payments validate function for webform_client_form.
 */
function webform_payments_webform_client_form_validate($form_id, $form_values) {
  
}

/**
 * webform_payments submit function for webform_client_form.
 */
function webform_payments_webform_client_form_submit($form_id, $form_values) {
  if (arg(2) == "edit") return;
  $submitted = $form_values['submitted'];
  $total = 0;
  $nid = array_pop(explode('_', $form_values['form_id']));
  foreach ($submitted as $form_key => $value) {
    $use = db_result(db_query("SELECT wp.webform_use FROM {webform_payments} wp INNER JOIN {webform_component} wc ON wp.webform_component = wc.cid WHERE wc.form_key = '%s' AND wp.nid = %d", $form_key, $nid));
    if ($use && is_numeric($value)) {
      $total += $value; 
    }
    if ($use && is_array($value)) {
      foreach($value as $amount => $checked) {
        if($checked && is_numeric($amount)) {
          $total += $amount;
        }
      }
    }
  }
  
  $form_values['webform_payments_total'] = $total;
  return webform_payments_send($form_values);
}

/*************************************************************
 * HELPER FUNCTIONS
 */

/**
 * Detects if a "$" sign was entered with the text and eliminates it.
 */
function _webform_payments_process_amount_value($amount) {
  $amount = explode('_', $amount);
  $amount = $amount[0];
  if (substr($amount, 0, 1) == "$") {
    $amount = substr($amount, 1);
  }

  return $amount;
}

function webform_payments_send($form_values) {
  $string  = 'totals='.urlencode($form_values['webform_payments_total']).'&';
  $string .= 'paypal_title='.urlencode($form_values['webform_payments_title']).'&';
  drupal_goto('webform_payments/gateway', $string);  
}

